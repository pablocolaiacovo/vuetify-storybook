# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.7](https://gitlab.com/pablocolaiacovo/vuetify-storybook/compare/v0.1.6...v0.1.7) (2020-08-12)


### Features

* **.storybook/main.js:** removed commented out addon ([c125f33](https://gitlab.com/pablocolaiacovo/vuetify-storybook/commit/c125f339e1ccf51852e69a0dec8a6b20f0a2bf23))

### [0.1.6](https://gitlab.com/pablocolaiacovo/vuetify-storybook/compare/v0.1.5...v0.1.6) (2020-08-12)


### Bug Fixes

* **index.stories.js:** removed commented out code ([c138e75](https://gitlab.com/pablocolaiacovo/vuetify-storybook/commit/c138e757a3923141ee9997b6367b205ddc51bd86))

### [0.1.5](https://gitlab.com/pablocolaiacovo/vuetify-storybook/compare/v0.1.4...v0.1.5) (2020-08-12)


### Features

* migrated `knobs` to `controls` addon ([664e231](https://gitlab.com/pablocolaiacovo/vuetify-storybook/commit/664e231fb79af0f8fcb1311d844738c49e017f2a))

### [0.1.4](https://gitlab.com/pablocolaiacovo/vuetify-storybook/compare/v0.1.3...v0.1.4) (2020-08-12)


### Features

* upgraded sb to v6 and added some extra addons ([786788e](https://gitlab.com/pablocolaiacovo/vuetify-storybook/commit/786788efebb4907fe48aa5e60a91c4aa0989a66a))

### [0.1.3](https://gitlab.com/pablocolaiacovo/vuetify-storybook/compare/v0.1.2...v0.1.3) (2020-08-11)


### Bug Fixes

* **package.json:** removed `git add` from pre-commit hook ([0277ea6](https://gitlab.com/pablocolaiacovo/vuetify-storybook/commit/0277ea6af5df1d33296bb4a25291d1b8f8966175))

### [0.1.2](https://gitlab.com/pablocolaiacovo/vuetify-storybook/compare/v0.1.1...v0.1.2) (2020-08-11)


### Features

* **package.json:** added `standard-release` to manage releases ([47d7df7](https://gitlab.com/pablocolaiacovo/vuetify-storybook/commit/47d7df758149f34164b602e710876d07f6bba32f))

### 0.1.1 (2020-08-11)
