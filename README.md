# Vuetify Storybook

Requirements:

- [x] Integrate Vuetify into a SPA
- [x] Add Storybook + Vuetify
- [x] Build & Deploy

## Commit & Release

### Commit

```sh
$ git add .
$ yarn commit
```

Reference: https://github.com/commitizen/cz-cli

### Release

```sh
$ yarn release
$ git push --follow-tags origin master
```

Reference: https://github.com/conventional-changelog/standard-version

## Vue SPA + Vuetify

https://vuetify.z13.web.core.windows.net/

```sh
$ yarn serve
```

## Storybook + Vuetify

https://vuetifysb.z13.web.core.windows.net/?path=/story/button--with-jsx

```sh
$ yarn storybook:serve
```

## References

- [Vuetify](https://vuetifyjs.com/en/getting-started/quick-start/#vue-cli-install)
- [Vuetify + Storybook](https://github.com/vuetifyjs/vue-cli-plugins/tree/master/packages/vue-cli-plugin-vuetify-storybook)
- [Conventional Commits](https://www.conventionalcommits.org/)
