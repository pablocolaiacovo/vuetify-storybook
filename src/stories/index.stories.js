import { action } from "@storybook/addon-actions";
import { linkTo } from "@storybook/addon-links";

import VuetifyButton from "../components/VuetifyButton.vue";

export default {
  component: VueitfyButtonWithText,
  title: "Buttons",
};

const VuetifyButtonTemplate = (args) => ({
  props: Object.keys(args),
  components: { VuetifyButton },
  template: `<vuetify-button :disabled="disabled" :depressed="depressed" @click="onClick" :dark="dark" :color="color || colorType">{{ label }}</vuetify-button>`,
});

export const VueitfyButtonWithText = VuetifyButtonTemplate.bind({});
VueitfyButtonWithText.args = {
  label: "Secondary Text",
  disabled: false,
  depressed: false,
  onClick: action("clicked"),
  dark: false,
  colorType: `primary`,
  color: ``,
};
VueitfyButtonWithText.argTypes = {
  label: {
    description: `This is a description`,
    table: {
      defaultValue: {
        summary: `default value summary`,
      },
    },
    control: {
      type: `text`,
    },
  },
  disabled: {
    description: `Disabled effect toggle`,
  },
  depressed: {
    description: `Depressed effect toggle`,
  },
  dark: {
    description: `Turn on/off dark mode`,
  },
  colorType: {
    description: `Predefined color types`,
  },
  color: {
    description: `Override colorType prop with a custom color`,
    control: {
      type: `color`,
    },
  },
};

export const VuetifyButtonWithLinkTo = VuetifyButtonTemplate.bind({});
VuetifyButtonWithLinkTo.args = {
  label: `Link to "With Some Emoji"`,
  onClick: linkTo("Buttons", "Vuetify Button With Some Emoji"),
};

export const VuetifyButtonWithSomeEmoji = VuetifyButtonTemplate.bind({});
VuetifyButtonWithSomeEmoji.args = {
  label: `😀 😎 👍 💯`,
};
