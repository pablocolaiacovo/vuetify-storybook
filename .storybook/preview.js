import { addDecorator } from "@storybook/vue";
import { withVuetify } from "./addon-vuetify";

addDecorator(withVuetify);

export const parameters = {
  controls: { expanded: true },
};
